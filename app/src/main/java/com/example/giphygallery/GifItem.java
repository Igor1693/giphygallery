package com.example.giphygallery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GifItem {
    @SerializedName("images")
    @Expose
    public Images images;

    public class Images {
        @SerializedName("fixed_height")
        @Expose
        public ImagesData data;

        public class ImagesData {
            @SerializedName("url")
            @Expose
            public String url;

            public String getUrl() {
                return url;
            }
        }

        public ImagesData getData() {
            return data;
        }
    }

    public Images getImages() {
        return images;
    }
}