package com.example.giphygallery;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiClient {
    @GET("/v1/gifs/trending")
    Call<GifList> getGifList(@Query("api_key") String key, @Query("limit") int limit, @Query("offset") int offset);

    @GET("/v1/gifs/search")
    Call<GifList> search(@Query("api_key") String key, @Query("q") String query, @Query("limit") int limit, @Query("offset") int offset);
}
