package com.example.giphygallery;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;

public class GifAdapter extends RecyclerView.Adapter<GifAdapter.GifHolder> {
    private List<GifItem> list = new ArrayList<>();
    private Context context;

    public void add(GifItem gifItem) {
        list.add(gifItem);
        notifyDataSetChanged();
    }

    public GifAdapter (Context context) {
        this.context = context;
    }

    @Override
    public GifAdapter.GifHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.gallery_item, parent, false);

        return new GifHolder(view);
    }

    @Override
    public void onBindViewHolder(GifHolder gifHolder, int position) {
        gifHolder.bindGifItem(list.get(position));

        Glide.with(context)
                .asGif()
                .load(list.get(position).getImages().getData().getUrl())
                .into(gifHolder.mGifImageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class GifHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mGifImageView;
        private GifItem mGifItem;

        public GifHolder(View gifView) {
            super(gifView);

            mGifImageView = gifView.findViewById(R.id.gif_image_view);
            mGifImageView.setOnClickListener(this);
        }

        public void bindGifItem(GifItem gifItem) {
            mGifItem = gifItem;
        }

        @Override
        public void onClick(View v) {
            String url = mGifItem.getImages().getData().getUrl();
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, url);
            context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string.share_gif)));

        }
    }
}
