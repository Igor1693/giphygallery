package com.example.giphygallery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GifList {
    @SerializedName("data")
    @Expose
    private List<GifItem> data;

    public List<GifItem> getData() {
        return data;
    }
}
