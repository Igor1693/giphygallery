package com.example.giphygallery;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mGiphyRecyclerView;
    private GifAdapter gifAdapter;

    public int NUM_ITEMS_PAGE = 18;
    private List<GifItem> gifList;
    private int paginationCounter = 0;
    private int offset = 0;
    private String searchQuery;
    private boolean isSearching = false;
    private boolean isFirstPage = true;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.giphy_gallery);

        mGiphyRecyclerView = findViewById(R.id.giphy_recycler_view);
        mGiphyRecyclerView.setHasFixedSize(true);
        mGiphyRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        mGiphyRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                GridLayoutManager gridLayoutManager = GridLayoutManager.class.cast(mGiphyRecyclerView.getLayoutManager());

                int totalItemCount = gridLayoutManager.getItemCount();
                int lastVisible = gridLayoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                if (!isLoading && totalItemCount > 0 && endHasBeenReached) {
                    if (!isSearching) {
                        loadGifs();
                    } else {
                        searchGifs(searchQuery);
                    }
                }
            }
        });

        if (!isSearching && isFirstPage) {
            loadGifs();
        }
    }

    public void loadGifs() {
        isLoading = true;
        RestClient.getInstance()
                .getApi()
                .getGifList(RestClient.API_KEY, NUM_ITEMS_PAGE, offset)
                .enqueue(new Callback<GifList>() {
                    @Override
                    public void onResponse(Call<GifList> call, Response<GifList> response) {
                        List<GifItem> list = response.body().getData();
                        if (list != null) {
                            if (isFirstPage) {
                                paginationCounter = 0;
                                gifList = list;
                                isFirstPage = false;
                                setupAdapter();
                            } else {
                                gifList.addAll(list);
                            }
                            addItemToAdapter();
                            offset = gifList.size();
                        }
                        isLoading = false;
                    }

                    @Override
                    public void onFailure(Call<GifList> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void searchGifs(String searchQuery) {
        isLoading = true;
        RestClient.getInstance()
                .getApi()
                .search(RestClient.API_KEY, searchQuery, NUM_ITEMS_PAGE, offset)
                .enqueue(new Callback<GifList>() {
                    @Override
                    public void onResponse(Call<GifList> call, Response<GifList> response) {
                        List<GifItem> list = response.body().getData();
                        if (list != null) {
                            if (isFirstPage) {
                                paginationCounter = 0;
                                gifList = list;
                                isFirstPage = false;
                                setupAdapter();
                            } else {
                                gifList.addAll(list);
                            }
                            addItemToAdapter();
                            offset = gifList.size();
                        }
                        isLoading = false;
                    }

                    @Override
                    public void onFailure(Call<GifList> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setupAdapter() {
        gifAdapter = new GifAdapter(MainActivity.this);
        mGiphyRecyclerView.setAdapter(gifAdapter);
    }

    private void addItemToAdapter() {
        paginationCounter += NUM_ITEMS_PAGE;
        for (int i = paginationCounter - NUM_ITEMS_PAGE; i < paginationCounter; i++) {
            if (i < gifList.size() && gifList.get(i) != null)
                gifAdapter.add(gifList.get(i));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.giphy_gallery, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
               isSearching = true;
               isFirstPage = true;
               searchQuery = s;
               searchGifs(searchQuery);

               return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                isSearching = false;
                isFirstPage = true;
                setupAdapter();
                loadGifs();

                return false;
            }
        });

        return true;
    }

}
