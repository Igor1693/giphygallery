package com.example.giphygallery;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static RestClient mInstance;
    public static final String BASE_URL = "http://api.giphy.com";
    public static final String API_KEY = "0GzyRqGgDcf3U3gh8lz2mkoOPjswFCgI";
    private Retrofit mRetrofit;

    private RestClient() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RestClient getInstance() {
        if (mInstance == null) {
            mInstance = new RestClient();
        }
        return mInstance;
    }

    public ApiClient getApi() {
        return mRetrofit.create(ApiClient.class);
    }
}
